package readability;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/**
 * Readability class is a GUI page for WordCounter to work on.
 * @author Chinatip Vichian
 *
 */
public class Readability extends JFrame implements Runnable{
	/* TextArea for showing selected file */
	private JTextField text1;
	/* JFrame for open dialog of JFileChooser*/
	private JFrame frame = new JFrame();
	/* JFileChooser for browse file */
	private JFileChooser fileChooser = new JFileChooser();
	/* Button for open JFileChooser */
	private JButton browse;
	/* TextArea that shows information from WordConter */
	private JTextArea text2;
	/* Constructor class for Readability */
	public Readability(){
		super("File Counter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
		this.pack();
	}
	/* Components of this class */
	public void initComponents(){
		Container container = new Container();
		super.setContentPane(container);
		container.setLayout(new BoxLayout(container,BoxLayout.Y_AXIS));
		JPanel panel1 = new JPanel();
		panel1.setLayout(new FlowLayout());
		JLabel label1 = new JLabel("File  or  URL name:    ");
		text1 = new JTextField(15);
		browse = new JButton("Browse...");
		browse.addActionListener(new browseListener());
		JButton countButton = new JButton("Count");
		JButton clearButton = new JButton("Clear");
		panel1.add(label1);
		panel1.add(text1);
		panel1.add(browse);
		panel1.add(countButton);
		countButton.addActionListener(new countListener());
		clearButton.addActionListener(new clearListener());
		panel1.add(clearButton);
		JPanel panel2 = new JPanel();
		panel2.setLayout(new FlowLayout());
		text2 = new JTextArea(7,50);
		panel2.add(text2);
		container.add(panel1);
		container.add(panel2);
	}
	/** ActionListener class for browseButton
	 *  Open JfileCHooser
	 */
	class browseListener implements ActionListener{

		public void actionPerformed(ActionEvent e) {
			fileChooser.showOpenDialog(frame);
			text1.setText("file:/" + fileChooser.getSelectedFile().getAbsolutePath());
		}
	}
	/** ActionListener for clearButton
	 *  Clear all text in text1 and text2.
	 */
	class clearListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			text1.setText("");
			text2.setText("");
		}
	}
	/** ActionListener class for countButton
	 * Count words, syllables ,...., from selected file or URL and show output in text2.
	 */
	class countListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			text2.setText(getInformation());
		}
	}
	/* Run method that override from Runnable interface */
	@Override
	public void run() {
		this.setVisible(true);
	}
	/* Main method for running Readability */
	public static void main(String[] args){
		Readability read = new Readability();
		read.run();
	}
	/**
	 * Get information from WordCounter
	 * @return String of information from WordCounter
	 */
	public String getInformation(){
		WordCounter counter = new WordCounter();
		URL url = null;

		String d="";
		try {
			url = new URL(text1.getText());
			double words = counter.countWords(url.openStream());

			d += "Filename: "+text1.getText() +"\n";
			d += "Number of Syllables: "+counter.getSyllableCount()+"\n";

			d += "Number of Words: " + (int)words +"\n";
			d += "Number of Sentences: "+ counter.getSentences()+"\n";
			double fleshIndex = 206.835 - 84.6*((double)counter.getSyllableCount()/words)
					- 1.015*(words / (double)counter.getSentences() );
			d += String.format("Flesch Index: %.2f\n",fleshIndex  );
			Index index = Index.GRADE5;
			d += "Readability: "+ index.search(fleshIndex) + "\n";
			text2.setText(d);


		} catch (IOException e1) {

		}
		return d;
	}
}