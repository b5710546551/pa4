package readability;
/**
 * Enum for checking readability index
 * @author Chinatip Vichian
 *
 */
public enum Index {
	GRADE4("4th grade student",101,1000),
	GRADE5("5th grade student",91,100),
	GRADE6("6th grade student",81,90),
	GRADE7("7th grade student",71,80),
	GRADE8("8th grade student",66,70),
	GRADE9("9th grade student",61,65),
	HIGHSCHOOL("High school student",51,60),
	COLLEGESTUDENT("College student",31,50),
	COLLEGEGRADUATE("College graduate",0,30),
	ADVANCEDEGREE("Advanced degree graduate",-100,0);
	/*  String of name of the index*/
	public String name;	
	/* Minimum of each index*/
	public int min;
	/* Maximum of each index */
	public int max;
	/* Constructor of Index */
	Index(String name, int min, int max){
		this.name = name;
		this.min = min;
		this.max = max;
	}
	/**
	 * Get name of index
	 * @return name of Index
 	 */
	public String getName(){
		return this.name;
	}
	/**
	 * Get minimum of index
	 * @return minimum of index
	 */
	public int getMin(){
		return this.min;
	}
	/**
	 * Get maximum of index
	 * @return maximum of index
	 */
	public int getMax(){
		return this.max;
	}
	/**
	 * Search 
	 * @param value is double that come to check what kind of index is it
	 * @return name of index that in range of that value
	 */
	public String search(double value){
		if(value>=101)
			return Index.GRADE4.getName();
		if(value<0)
			return Index.ADVANCEDEGREE.getName();
		int i=0;

		Index[] arr = values();
		while(i<arr.length && !(value<=arr[i].getMax() && value>=arr[i].getMin()) ){
			i++;
		}
		return arr[i].getName();
	}
}
